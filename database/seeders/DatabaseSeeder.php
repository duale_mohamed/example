<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Comment;
use App\Models\PostApp;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        Category::truncate();
        PostApp::truncate();
        Comment::truncate();

        PostApp::factory(5)->create();


    }
}
